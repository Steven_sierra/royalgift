class OrderMailer < ActionMailer::Base
	default from: "royalgiftbogota@gmail.com"

	def download_order(order)
		@order = order
		mail(to: @order.email, subject:"Gracias por adquirir nuestros servicios")
		
	end
end