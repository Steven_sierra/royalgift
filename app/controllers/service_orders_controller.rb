class ServiceOrdersController < ApplicationController

  def index
  	
  end

  def create
  	@order = ServiceOrder.new(email: params[:email],
                               de: params[:de], 
                               para: params[:para],
                               telefono: params[:telefono],
                               direccion: params[:direccion],
                               hora: params[:hora],
                               shopping_cart_id: @shopping_cart.id)

  	if @order.save
  		redirect_to "/confirm", notice: "Se guardo la orden con exito"
      return
  	else
  		redirect_to "/carrito", alert: "No se pudo crear la orden "
  	end
  end
  
  def order_params
  	
  	
  end

 
end
