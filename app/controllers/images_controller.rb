class ImagesController < ApplicationController
   
  before_action :authenticate_user!
  before_action :set_image, only:[:destroy]
  before_action :set_product, only:[:destroy]
  before_action :authenticate_owner!


  def show
  	
  end

  def new
  end  

  def create
  	@image = Image.new(image_params)
  	if @image.save
  		redirect_to @image.product, notice: "Se guardo la imagen con exito"
  	else
  		redirect_to @product, alert: "No se pudo guardar la imagen "
  	end
  end

  def destroy
    @image.destroy
    redirect_to @product
  end

  private
    def set_image
      @image = Image.find(params[:id])
    end

    def set_product
      @product = @image.product
    end

  	def authenticate_owner!

      if params.has_key? :image
  		  @product = Product.find(params[:image][:product_id])
      end

  		if @product.user != current_user || @product.nil? 
  			redirect_to root_path, notice: "No puedes editar este producto"
  			return
  		end
  	end

  def image_params
  	params.require(:image).permit(:product_id,:image)
  	
  end

end
