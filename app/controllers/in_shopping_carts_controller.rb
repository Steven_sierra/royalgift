class InShoppingCartsController < ApplicationController

	def create
		#agregar a un carrito de compras
		in_shopping_cart = InShoppingCart.new(product_id: params[:product_id], shopping_cart: @shopping_cart)
		if in_shopping_cart.save
			redirect_to product_path(id: params[:product_id]), notice: "El producto fue afregado a tu carrito"
		else
			redirect_to products_path(id: params[:product_id]), notice: "No pudimos agregar el producto al carrito"
		end

	end

	def destroy
		@in_shopping_cart = InShoppingCart.find(params[:id])
		@in_shopping_cart.destroy

		redirect_to carrito_path
		

	end
end
