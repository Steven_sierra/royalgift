class PaymentsController < ApplicationController

  def checkout
    @my_payment = MyPayment.find_by(paypal_id: params[:paymentId])
    if @my_payment.nil?
      redirect_to "/carrito"
    else
      Stores::Paypal.checkout(params[:PayerID],params[:paymentId]) do
        @my_payment.pay!
        redirect_to ok_path, notice: "Se proceso satisfactoriamente con paypal"
        return
      end
      redirect_to carrito_path, alert:"Hubo un error en la transaccion"
    end

  end

  def process_card
    paypal_helper = Stores::Paypal.new(shopping_cart: @shopping_cart,
                                        return_url: checkout_url,
                                        cancel_url: carrito_url,
                                        items: @shopping_cart.items,
                                        total: @shopping_cart.total) 
      if paypal_helper.process_card(params).create
        @my_payment = MyPayment.create!(paypal_id: paypal_helper.payment.id, 
                                    ip: request.remote_ip,
                                    email: params[:email],
                                    shopping_cart_id: cookies[:shopping_cart_id])
        @my_payment.pay!
        redirect_to ok_path, notice: "Se proceso satisfactoriamente tu pago"
      else
        redirect_to carrito_path, alert: paypal_helper.payment.error
      end
  end

  def create
    paypal_helper = Stores::Paypal.new(shopping_cart: @shopping_cart,
                                        return_url: checkout_url,
                                        cancel_url: carrito_url,
                                        items: @shopping_cart.items,
                                        total: @shopping_cart.total)

  	if paypal_helper.process_payment.create
      @my_payment = MyPayment.create!(paypal_id: paypal_helper.payment.id, 
                                    ip: request.remote_ip,
                                    shopping_cart_id: cookies[:shopping_cart_id] )
  		redirect_to paypal_helper.payment.links.find{|v| v.method == "REDIRECT"}.href
  	else
  		raise paypal_helper.payment.error.to_yaml
  	end
  end

end
















