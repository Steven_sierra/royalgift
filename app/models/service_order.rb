# == Schema Information
#
# Table name: service_orders
#
#  id               :integer          not null, primary key
#  para             :string
#  de               :string
#  telefono         :string
#  date             :date
#  email            :string
#  hora             :string
#  direccion        :string
#  shopping_cart_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#


class ServiceOrder < ApplicationRecord
	
	belongs_to :shopping_cart
	after_create :send_email
	has_many :products, through: :shopping_cart

private
	def send_email
		OrderMailer.download_order(self).deliver_now
		
	end

end
