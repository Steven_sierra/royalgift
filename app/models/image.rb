# == Schema Information
#
# Table name: images
#
#  id                 :integer          not null, primary key
#  product_id         :integer
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Image < ApplicationRecord
	belongs_to :product
	validates :image, attachment_presence: true

	has_attached_file :image, styles: { medium:"300x400", thumb:"100x100"}
	validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
end
