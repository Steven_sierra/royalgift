# == Schema Information
#
# Table name: contac_emails
#
#  id         :integer          not null, primary key
#  email      :string
#  ip         :string
#  state      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ContacEmail < ApplicationRecord
	validates_presence_of :email
	validates_uniqueness_of :email
	validates_format_of :email, with: Devise::email_regexp
end
