Rails.application.routes.draw do


  get 'shopping_carts/show'

  get "/carrito", to: "shopping_carts#show"
  get "/confirm", to: "service_orders#index"
  get "/add/:product_id", as: :add_to_cart, to:"in_shopping_carts#create"
  get "/delete/:id", as: :delete_to_cart, to:"in_shopping_carts#destroy"
  get "/checkout", to: "payments#checkout"
  get "/ok", to: "welcome#payment_succes"
  get "/who", to: "info#who"
 



  resources :products
  resources :in_shopping_carts, only: [:create, :destroy]
  resources :images, only: [:create, :destroy, :new]
  resources :service_orders, only: [:create,:new]

  devise_for :users

  post "/emails/create", as: :create_email
  post "pay", to: "payments#create"
  post "/payments/cards", to:"payments#process_card"
  
  
  authenticated :user do  
  	root 'products#index'
  end



  unauthenticated :user do
   	devise_scope :user do 
  		root "welcome#unregistered", as: :unregistered_root
  	end	
  end
end
