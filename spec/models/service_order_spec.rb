# == Schema Information
#
# Table name: service_orders
#
#  id               :integer          not null, primary key
#  para             :string
#  de               :string
#  telefono         :string
#  date             :date
#  email            :string
#  hora             :string
#  direccion        :string
#  shopping_cart_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

require 'rails_helper'

RSpec.describe ServiceOrder, type: :model do
  it "should send email" do 
  	expect{
  		FactoryGirl.create(:service_order)
  	}.to change(ActionMailer::Base.deliveries,:count).by(1)
  end
end
