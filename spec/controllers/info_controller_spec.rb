require 'rails_helper'

RSpec.describe InfoController, type: :controller do

  describe "GET #who" do
    it "returns http success" do
      get :who
      expect(response).to have_http_status(:success)
    end
  end

end
