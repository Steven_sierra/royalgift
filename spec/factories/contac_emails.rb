# == Schema Information
#
# Table name: contac_emails
#
#  id         :integer          not null, primary key
#  email      :string
#  ip         :string
#  state      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :contac_email do
    email "MyString"
    ip "MyString"
    state 1
  end
end
