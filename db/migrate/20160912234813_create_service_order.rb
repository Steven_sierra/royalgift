class CreateServiceOrder < ActiveRecord::Migration[5.0]
  def change
    create_table :service_orders do |t|
      t.string :para
      t.string :de
      t.string :telefono
      t.date :date
      t.string :email
      t.string :hora
      t.string :direccion
      t.references :shopping_cart, index:true, foreign_key:true

      t.timestamps
    end
  end
end
